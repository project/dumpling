<?php

/**
 * Implements hook_drush_command().
 */
function dumpling_drush_command() {
  $items = array();
  $defaults = array(
    'options' => array(
      'processor' => 'The name of a processor to use. Defaults to dbdump.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );
  $items['dumpling-export-config'] = array(
    'description' => "Export all configuration from this site.",
    'aliases' => array('decf'),
    'options' => array(
      'processor' => 'The name of a processor to use. Defaults to dbdump.',
      'exlude-files' => 'Exclude files folder',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );
  $items['dumpling-import-config'] = array(
    'aliases' => array('dicf'),
    'description' => "Import configuration into this drupal site.",
  ) + $defaults;
  $items['dumpling-export-content'] = array(
    'aliases' => array('deco'),
    'description' => "Export all content from this site.",
  ) + $defaults;
  $items['dumpling-import-content'] = array(
    'aliases' => array('dico'),
    'description' => "Imports exported content to this site.",
  ) + $defaults;
  return $items;
}

/**
 * Implements hook_drush_help().
 */
function dumpling_drush_help($section) {
  switch ($section) {
    case 'drush:dumpling-export':
      return dt("Exports all configuration tables to a dumped database files");
    case 'drush:dumpling-import':
      return dt("Imports all configuration tables from dumped files");
    case 'drush:dumpling-import-content':
      return dt("Imports all content tables from dumped files");
  }
}

/**
 * Command callback for dumpling-export.
 */
function drush_dumpling_export() {
  $path = dumpling_get_path();
  if (!file_exists($path)) {
    mkdir($path);
  }
  $processor = dumpling_get_processor(dumpling_get_option('processor', 'php'));
  if (!$processor) {
    drush_set_error('processor', 'Invalid processor');
    return;
  }
  // Export the schema.
  $processor->exportSchema($path);
  dumpling_export_config($processor, $path);
}

function drush_dumpling_export_content() {
  $path = dumpling_get_path();
  if (!file_exists($path)) {
    mkdir($path);
  }
  $processor = dumpling_get_processor(dumpling_get_option('processor', 'php'));
  if (!$processor) {
    drush_set_error('processor', 'Invalid processor');
    return;
  }
  dumpling_export_content($processor, $path);
  if (!dumpling_get_option('exclude-files', FALSE)) {
    dumpling_export_files($path);
  }
}

/**
 * Export public files to a path.
 */
function dumpling_export_files($path) {
  $file_path = $path . '/files';
  if (!file_exists($file_path)) {
    mkdir($file_path);
  }
  $excludes = module_invoke_all('dumpling_exclude_files');
  $exclude_strings = array();
  foreach ($excludes as $exclude) {
    $exclude_strings[] = '--exclude=' . $exclude;
  }

  $drupal_path = variable_get('file_public_path', conf_path() . '/files');
  drush_shell_exec("rsync -r " . implode(' ', $exclude_strings) . " $drupal_path/* $file_path");
}

/**
 * Import public files.
 */
function dumpling_import_files($path) {
  $file_path = $path . '/files';
  if (!file_exists($file_path)) {
    return FALSE;
  }
  $drupal_path = variable_get('file_public_path', conf_path() . '/files');
  drush_shell_exec("cp -R $file_path/* $drupal_path/.");
}

/**
 * Import tables.
 */
function drush_dumpling_import() {
  $connect_string = _drush_sql_connect();
  $path = dumpling_get_path();
  $processor = dumpling_get_processor(dumpling_get_option('processor', 'php'));
  if (!$processor) {
    drush_set_error('processor', 'Invalid processor');
    return;
  }
  drush_log(dt('Importing schema definition'), 'status');
  $processor->importSchema($path);
  $tables = dumpling_get_config_tables();
  $table_path = $path . '/config';
  foreach ($tables as $table) {
    $processor->import($table_path, $table);
  }
  drupal_flush_all_caches();
}

/**
 * Import content tables.
 */
function drush_dumpling_import_content() {
  $path = dumpling_get_path();
  $tables = dumpling_get_content_tables();
  $table_path = $path . '/content';
  $processor = dumpling_get_processor(dumpling_get_option('processor', 'php'));
  if (!$processor) {
    drush_set_error('processor', 'Invalid processor');
    return;
  }
  foreach ($tables as $table) {
    drush_log(dt('Importing table !table', array('!table' => $table)), 'status');
    if (!$processor->import($table_path, $table)) {
      drush_log(dt('Failed to parse table file !table', array('!table' => $table)), 'error');
    }
  }
  dumpling_import_files($path);
  module_invoke_all('dumpling_post_content_import');
  drupal_flush_all_caches();
}

/**
 * Export all configuration tables.
 */
function dumpling_export_config($processor, $path) {
  $table_path = $path . '/config';
  if (!file_exists($table_path)) {
    mkdir($table_path);
  }
  $tables = dumpling_get_config_tables();
  foreach ($tables as $table) {
    drush_log(dt('Exporting !table', array('!table' => $table)), 'status');
    $processor->export($table_path, $table);
  }
}

/**
 * Export all content tables.
 */
function dumpling_export_content($processor, $path) {
  $table_path = $path . '/content';
  if (!file_exists($table_path)) {
    mkdir($table_path);
  }
  $tables = dumpling_get_content_tables();
  foreach ($tables as $table) {
    drush_log(dt('Exporting !table', array('!table' => $table)), 'status');
    $processor->export($table_path, $table);
  }
}

