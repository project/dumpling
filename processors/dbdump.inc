
<?php

/**
 * A processor that imports and exports using mysqldump.
 */
class DumplingDatabaseProcessor implements dumplingProcessor {

  function __construct() {
    $this->db_spec = _drush_sql_get_db_spec();
  }

  /**
   * Export the specified table.
   */
  function export($path, $table) {
    if (!file_exists($path)) {
      return FALSE;
    }
    $db_spec = $this->db_spec;
    $cmd = "mysqldump --skip-comments --user={$db_spec['username']} --password={$db_spec['password']}  --result-file {$path}/{$table}.sql {$db_spec['database']} $table";
    drush_shell_exec($cmd);

  }

  /**
   * Import the specified table.
   */
  function import($path, $table) {
    $connect_string = _drush_sql_connect();
    $table_path = $path . '/' .  $table . '.sql';
    if (file_exists($table_path)) {
      $output = '';
      drush_shell_exec($connect_string . ' < ' . $table_path, &$output);
    }
    return TRUE;
  }

  /**
   * Export the whole schema definition.
   */
  function exportSchema($path) {
    if (file_exists($path . '/schema.sql')) {
      unlink($path . '/schema.sql');
    }
    // Export the whole database schema as a reference.
    $db_spec = $this->db_spec;
    $database = $db_spec['database'];
    $exec = "mysqldump --skip-add-drop-table --skip-comments --skip-set-charset --no-data --add-locks --user={$db_spec['username']} --password={$db_spec['password']} --result-file {$path}/schema-unprocessed.sql";
    $exec .= ' ' . $db_spec['database'];
    $exec .= " && sed 's/CREATE TABLE/CREATE TABLE IF NOT EXISTS/g' $path/schema-unprocessed.sql > $path/schema.sql";
    drush_shell_exec($exec);
    unlink($path . '/schema-unprocessed.sql');
    return $this;
  }

  function importSchema($path) {
    $connect_string = _drush_sql_connect();
    if (file_exists($path . '/schema.sql')) {
      $output = '';
      drush_shell_exec($connect_string . ' < ' . $path . '/schema.sql', $output);
    }
  }
}
