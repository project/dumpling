<?php

/**
 * A processor that imports and exports using json.
 * This works very poorly with serialized PHP.
 */
class DumplingJSONProcessor implements dumplingProcessor {

  function __construct() {
    $this->db_spec = _drush_sql_get_db_spec();
  }

  /**
   * Export the specified table.
   */
  function export($path, $table) {
    if (!file_exists($path) || !db_table_exists($table)) {
      return FALSE;
    }
    $result = db_select($table, 't')->fields('t')->execute();
    $table_data = array();
    foreach ($result as $row) {
      $table_data[] = $row;
    }
    if (count($table_data)) {
      if ($data = drupal_json_encode($table_data)) {
        file_put_contents($path . '/' . $table . '.json', $data);
      }
      else {
        drush_log(dt('Failed to export table file !table', array('!table' => $table)), 'error');
      }
    }
  }

  /**
   * Import the specified table.
   */
  function import($path, $table) {
    $table_path = $path . '/' .  $table . '.json';
    if (file_exists($table_path)) {
      $output = '';
      if ($data = drupal_json_decode(file_get_contents($table_path))) {
        db_query("TRUNCATE TABLE $table");
        foreach ($data as $row) {
          drupal_write_record($table, $row);
        }
      }
      else {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Export the whole schema definition.
   */
  function exportSchema($path) {
    if (file_exists($path . '/schema.json')) {
      unlink($path . '/schema.json');
    }
    $schema = drupal_get_schema();
    file_put_contents($path . '/schema.json', drupal_json_encode($schema));
  }

  function importSchema($path) {
    if (!file_exists($path . '/schema.json')) {
      return FALSE;
    }
    $schema = drupal_json_decode(file_get_contents($path . '/schema.json'));
    if ($schema) {
      $schema = objectToArray($schema);
      foreach ($schema as $table => $definition) {
        if (!db_table_exists($table)) {
          db_create_table($schema);
        }
      }
    }
  }

  protected function objectToArray($object) {
    if (!is_object($object) && !is_array($object)) {
      return $object;
    }
    if (is_object( $object)) {
      $object = get_object_vars( $object );
    }
    return array_map('objectToArray', $object);
  }

  protected function isSerialized($val){
    if (!is_string($val)){ return false; }
    if (trim($val) == "") { return false; }
    if (preg_match("/^(i|s|a|o|d):(.*);/si",$val)) { return true; }
    return false;
  }
}
