<?php

/**
 * A processor that imports and exports using PHP.
 * Extremely memory intensive!
 */
class DumplingPHPProcessor implements DumplingProcessor {

  /**
   * Export the specified table.
   */
  function export($path, $table) {
    if (!file_exists($path) || !db_table_exists($table)) {
      return FALSE;
    }
    $result = db_select($table, 't')->fields('t')->execute();
    $table_data = array();
    $serialized_cols = array();
    foreach ($result as $row) {
      // Not all modules (not even core) declare all columns
      // as serialized as they should, so let's make our own check.
      foreach ($row as $name => $data) {
        if ($this->isSerialized($data)) {
          $row->{$name} = unserialize($data);
          $serialized_cols[$name] = $name;
        }
      }
      $table_data[] = $row;
    }
    if (!$this->exportPHP($path, $table, $table_data, $serialized_cols)) {
      return FALSE;
    }
  }

  /**
   * Import the specified table.
   */
  function import($path, $table) {
    $table_path = $path . '/' .  $table . '.php';
    if (file_exists($table_path)) {
      require_once($table_path);
      $fn = 'dumpling_table_' . $table;
      if (function_exists($fn) && db_table_exists($table)) {
        $schema = drupal_get_schema($table);
        $data = $fn();
        db_query("TRUNCATE TABLE $table");
        foreach ($data['data'] as $row) {
          foreach ($data['serialized_cols'] as $col) {
            // Serialize columns that neglected to define that they
            // should be serialized.
            if (isset($row->{$col}) && empty($schema['fields'][$col]['serialize'])) {
              $row->{$col} = serialize($row->{$col});
            }
          }
          drupal_write_record($table, $row);
        }
      }
      else {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Export the whole schema definition.
   */
  function exportSchema($path) {
    if (file_exists($path . '/schema.php')) {
      unlink($path . '/schema.php');
    }
    $schema = drupal_get_schema();
    $this->exportPHP($path, 'schema', $schema, 'schema');
  }

  function importSchema($path) {
    if (!file_exists($path . '/schema.php')) {
      return FALSE;
    }
    require_once($path . '/schema.php');
    $fn = 'dumpling_table_schema';
    if (function_exists($fn)) {
      $schema = $fn();
    }
    foreach ($schema as $table => $definition) {
      if (!db_table_exists($table)) {
        db_create_table($schema);
      }
    }
  }

  protected function objectToArray($object) {
    if (!is_object($object) && !is_array($object)) {
      return $object;
    }
    if (is_object( $object)) {
      $object = get_object_vars( $object );
    }
    return array_map('objectToArray', $object);
  }

  /**
   * Check if a value is serialized. Ugly as hell.
   */
  protected function isSerialized($val){
    $data = @unserialize($val);
    return ($data !== false || $val === 'b:0;');
  }

  function exportPHP($path, $file_name, $data, $serialized_cols = array(), $prefix = 'table') {
    ctools_include('export');
    $data = array('data' => $data, 'serialized_cols' => $serialized_cols);
    if ($php = ctools_var_export($data, '  ')) {
      $file_contents = "<?php\nfunction dumpling_{$prefix}_{$file_name}() {\n  return " . $php . ";\n}\n";
      file_put_contents($path . '/' . $file_name . '.php', $file_contents);
      return TRUE;
    }
    return FALSE;
  }
}
